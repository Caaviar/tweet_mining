#clé de telechargement
consumer_key <- "Oy694XsFKkQ0SwaJTSzFp5ymr"
consumer_secret <- "HCtbMMMS4NWUspHj2VYemJgo943ofqIHzGmD721jixaMsBNIHw"
access_token <- "3155557014-b43KtZqhNrOyZdlasXi3EwJjZGEXjPOmdMvJk77"
access_secret <- "hrv2lUlkGXZBS6A7YQxXUKw6SynCogytVCQXQJJx24huX"

#Créer une connexion avec Twitter
setup_twitter_oauth(consumer_key, consumer_secret, access_token, access_secret)

print("Chargement des données ...")

#Recuperation des données
start_time <- Sys.time()
tweets <- searchTwitter("rg19 + nadal",n=15000,lang="fr", since = '2019-06-01', until= '2019-06-09')
tweets2 <- searchTwitter("rg19 + federer",n=15000,lang="fr", since = '2019-06-01', until= '2019-06-09')

#Transformation en DF
df1 <- twListToDF(tweets)
df2 <- twListToDF(tweets2)

#DF pour l'etude descriptive
dfg1 <- df1 %>% filter(truncated == FALSE) %>% select(text, created, statusSource, screenName) %>% mutate(joueur = "nadal")
dfg2 <- df2 %>% filter(truncated == FALSE) %>% select(text, created, statusSource, screenName) %>% mutate(joueur = "federer")
dfg = rbind(dfg1, dfg2)
dfg = unique(dfg)

write.csv2(dfg, "./Get_Data/full_database.csv")

#DF pour l'etude de texte
dfn = df1 %>% filter(truncated == FALSE) %>% select(text) %>% mutate(joueur = "nadal")
dff = df2 %>% filter(truncated == FALSE) %>% select(text) %>% mutate(joueur = "federer")
df = rbind(dfn, dff)
df = unique(df)

write.csv2(df, "./Get_Data/text_database.csv")

end_time <- Sys.time()

Tdiff= as.vector(round(difftime(end_time, start_time),0) )
print(paste("Les données sont pretes, temps de calcul :" , Tdiff , "minutes"))

rm(df1, df2, dff, dfg1, dfg2, dfn, tweets, tweets2, Tdiff)

