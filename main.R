library(rlang)
#Installation et/ou chargement des package necessaire
load_or_install<-function(package_names)
{
  for(package_name in package_names)
  {
    if(!is_installed(package_name))
    {
      install.packages(package_name,repos="http://lib.stat.cmu.edu/R/CRAN")
    }
    library(package_name,character.only=TRUE,quietly=TRUE,verbose=FALSE)
  }
}
load_or_install(c("twitteR","dplyr","stringr", "tidytext", "tidyr", "tm", 
                  "wordcloud", "RColorBrewer", "DescTools", "igraph", "ggraph", 
                  "scales", "ggplot2", "plotly"))


tryCatch(
  {
    df = read.csv2("./Get_Data/text_database.csv")
    dfg = read.csv2("./Get_Data/full_database.csv")
  },
  error = function(cond){
    source("./Get_Data/script.R")
  }
)

source("./Descriptive_Analysis/descriptive.R")
source("./Text_Mining/textanalysis.R")
source("./Sentiment_Analysis/sentiment.R")
